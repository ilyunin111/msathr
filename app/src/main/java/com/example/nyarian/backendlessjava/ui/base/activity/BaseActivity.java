package com.example.nyarian.backendlessjava.ui.base.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;

import com.example.nyarian.backendlessjava.meta.BaseApplication;

import static timber.log.Timber.i;

public abstract class BaseActivity extends AppCompatActivity {

    protected BaseApplication getBaseApplication() {
        return BaseApplication.getInstance();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        i("onCreate(%s)", savedInstanceState);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onRestart() {
        i("onRestart()");
        super.onRestart();
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        i("onAttachFragment(%s)", fragment);
        super.onAttachFragment(fragment);
    }

    @Override
    public void onContentChanged() {
        i("onContentChanged()");
        super.onContentChanged();
    }

    @Override
    protected void onStart() {
        i("onStart()");
        super.onStart();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        i("onActivityResult(%d, %d, %s)", requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        i("onRestoreInstanceState(%s)", savedInstanceState);
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        i("onPostCreate(%s)", savedInstanceState);
        super.onPostCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        i("onResume()");
        super.onResume();
    }

    @Override
    protected void onPostResume() {
        i("onPostResume()");
        super.onPostResume();
    }

    @Override
    public void onAttachedToWindow() {
        i("onAttachedToWindow()");
        super.onAttachedToWindow();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        i("onCreateOptionsMenu(%s)", menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        i("onPrepareOptionsMenu(%s)", menu);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onUserInteraction() {
        i("onUserInteraction()");
        super.onUserInteraction();
    }

    @Override
    protected void onUserLeaveHint() {
        i("onUserLeaveHint()");
        super.onUserLeaveHint();
    }

    @Override
    protected void onPause() {
        i("onPause()");
        super.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        i("onSaveInstanceState(%s)", outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onStop() {
        i("onStop()");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        i("onDestroy()");
        super.onDestroy();
    }
}
