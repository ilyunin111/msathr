package com.example.nyarian.backendlessjava.ui.base.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.nyarian.backendlessjava.meta.BaseApplication;
import com.hannesdorfmann.mosby3.mvp.MvpFragment;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import static timber.log.Timber.i;

public abstract class BaseFragment<V extends MvpView, P extends MvpPresenter<V>>
        extends MvpFragment<V, P> {

    protected BaseApplication getApplication() {
        return BaseApplication.getInstance();
    }

    @Override
    public void onInflate(Context context, AttributeSet attrs, Bundle savedInstanceState) {
        i("onInflate(%s, %s, %s)", context, attrs, savedInstanceState);
        super.onInflate(context, attrs, savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        i("onAttach(%s)", activity);
        super.onAttach(activity);
    }

    @Override
    public void onAttach(Context context) {
        i("onAttach(%s)", context);
        super.onAttach(context);
    }

    @Override
    public void onAttachFragment(Fragment childFragment) {
        i("onAttachFragment(%s)", childFragment);
        super.onAttachFragment(childFragment);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        i("onCreate(%s)", savedInstanceState);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        i("onCreateView(%s, %s, %s)", inflater, container, savedInstanceState);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        i("onViewCreated(%s, %s)", view, savedInstanceState);
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        i("onActivityCreated(%s)", savedInstanceState);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        i("onViewStateRestored(%s)", savedInstanceState);
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onStart() {
        i("onStart()");
        super.onStart();
    }

    @Override
    public void onResume() {
        i("onResume()");
        super.onResume();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        i("onCreateOptionsMenu(%s, %s)", menu, inflater);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        i("onPrepareOptionsMenu(%s)", menu);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onPause() {
        i("onPause()");
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        i("onSaveInstanceState(%s)", outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        i("onStop()");
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        i("onDestroyView()");
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        i("onDestroy()");
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        i("onDetach()");
        super.onDetach();
    }

}
