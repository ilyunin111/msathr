package com.example.nyarian.backendlessjava.mvp.presenter.base;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class MvpBaseRxPresenter<T extends MvpView> extends MvpBasePresenter<T> {

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    protected void add(Disposable disposable) {
        compositeDisposable.add(disposable);
    }

    @Override
    public void destroy() {
        super.destroy();
        compositeDisposable.clear();
    }
}
