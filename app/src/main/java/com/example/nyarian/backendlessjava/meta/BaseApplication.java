package com.example.nyarian.backendlessjava.meta;

import android.app.Application;

import com.example.nyarian.backendlessjava.BuildConfig;
import com.example.nyarian.backendlessjava.di.component.DaggerPrimalComponent;
import com.example.nyarian.backendlessjava.di.component.PrimalComponent;
import com.example.nyarian.backendlessjava.di.module.ApplicationModule;
import com.squareup.leakcanary.LeakCanary;

import timber.log.Timber;

public class BaseApplication extends Application {

    private static BaseApplication instance = null;

    private PrimalComponent primalComponent;

    public static BaseApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        primalComponent = buildComponent();
        instance = this;
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
        if (LeakCanary.isInAnalyzerProcess(this)) {
            return;
        }
        LeakCanary.install(this);
    }

    private PrimalComponent buildComponent() {
        return DaggerPrimalComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public PrimalComponent getPrimalComponent() {
        return primalComponent;
    }
}
