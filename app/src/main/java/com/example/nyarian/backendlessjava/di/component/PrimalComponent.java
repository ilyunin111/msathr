package com.example.nyarian.backendlessjava.di.component;

import com.example.nyarian.backendlessjava.MainActivity;
import com.example.nyarian.backendlessjava.di.module.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class})
public interface PrimalComponent {
    void inject(MainActivity activity);
}
